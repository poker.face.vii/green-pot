import 'package:mobx/mobx.dart';
import 'package:mobx_provider/mobx_provider.dart';

part 'mainState.g.dart';

class MainState = _MainState with _$MainState;

abstract class _MainState extends MobxBase with Store {
  @observable
  String selected = 'hello';
  @observable
  String image = '';

  @action
  void selectCategory(cat) {
    selected = cat;
  }
  @action
  void getImage(imageSelected) {
    image = imageSelected;
  }

  @override
  void dispose() {}
}
