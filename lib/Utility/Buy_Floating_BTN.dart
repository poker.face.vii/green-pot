import 'package:flutter/material.dart';

class BuyFloatingActionButton extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(size.width, 0);
    path.quadraticBezierTo(-16, -16, 0, size.height);
    path.quadraticBezierTo(size.width + 16, size.height + 16, size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
