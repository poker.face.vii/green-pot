import 'package:flutter/material.dart';

class ProductDetailClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, 0);
    path.lineTo(0, size.height);
    // path.lineTo(67, size.height - 67);
    path.quadraticBezierTo(55, size.height-60, 70 ,size.height-60);
    path.lineTo(size.width - 70, size.height - 60);
    path.quadraticBezierTo(size.width -55, size.height-60, size.width ,size.height);
    // path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
