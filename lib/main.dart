//    -- Dependencies Library --
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//      -- Stores --
import 'package:green_pot/Mobx/mainState.dart';

//    -- Views Widget --
import 'package:green_pot/view/Testing.dart';
import 'view/Categories_view.dart';
import 'view/Main_Layout.dart';
import 'view/Product_details.dart';

void main() {
  runApp(
    GreenPot(),
  );
}

class GreenPot extends StatelessWidget {
  // This widget is the root of my application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider(
            create: (_) => MainState(),
          )
        ],
        child: MaterialApp(
          title: 'Green pot',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            appBarTheme: AppBarTheme(
                color: Colors.white,
                actionsIconTheme: IconThemeData(color: Colors.black),
                // centerTitle: true,
                elevation: 2,
                iconTheme: IconThemeData(color: Colors.black)),
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => MainView(),
            // '/': (context) => Test(),
            'underTest': (context) => UnderTest(),
            '/SelectedCat': (context) => CategorySelected(),
            '/ProductDetails': (context) => ProductDetails(),
          },
        ));
  }
}
