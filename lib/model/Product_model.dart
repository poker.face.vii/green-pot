import 'package:flutter/material.dart';

class Product {
  /// name of the product
  final String name;

  /// price of the product
  final int price;

  /// Description of the product
  final String description;

  /// Image of the product
  final Image image;

  /// owner of the product.the farm who wants to sell the product
  final String shopOwner;

  /// Product soil or recommended soil.
  final String soil;

  /// Available colors for the pot
  final List<Color> colors;

  /// size of the product
  final int size;

  /// Difficulty of keep and growth the product
  final int difficulty;

  /// Light of keep and growth the product
  final String light;

  /// Humidity of keep and growth the product
  final String humidity;

  /// Does this product non-toxic for the pet
  final bool petFriendly;

  /// Does this product clean the Air
  final String airCleaner;

  Product(
      {this.difficulty,
      this.light,
      this.humidity,
      this.petFriendly,
      this.airCleaner,
      this.name,
      this.price,
      this.shopOwner,
      this.image,
      this.description,
      this.soil,
      this.colors,
      this.size});
}
