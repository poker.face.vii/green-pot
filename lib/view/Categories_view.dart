import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:green_pot/Mobx/mainState.dart';
import 'package:provider/provider.dart';

class CategoriesView extends StatelessWidget {

  final List<String> catList = [
    'Flowers',
    'pot plants',
    'vegetables',
    'miniature',
    'inside',
    'cactus',
    'japanese miniature',
    'desk',
    'creeping'
  ];
  @override
  Widget build(BuildContext context) {
    //This the App main store
    final MainState store = Provider.of<MainState>(context, listen: false);
    double width = MediaQuery.of(context).size.width;
    // This is widget Build Bloc
    
      return GridView.builder(
        itemCount: 9,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            child: Material(
              elevation: 5.0,
              // shape: BeveledRectangleBorder(
              //   borderRadius: BorderRadius.only(topLeft: Radius.circular(46.0)),
              // ),
              child: Container(
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/SelectedCat');
                    store.selectCategory(catList[index]);
                    
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Image.asset('assets/images/cat$index.png',
                            width: (width / 2) - 40),
                      ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      Text(
                        '${catList[index]}',
                        style: TextStyle(fontFamily: 'Menu', fontSize: 15),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    
  }
}

class CategorySelected extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   final MainState store = Provider.of<MainState>(context, listen: false);
    return Observer(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(icon: Icon(FeatherIcons.shoppingCart), onPressed: null)
          ],
          title: Text(
            store.selected,
            style: TextStyle(color: Colors.black, fontFamily: 'Menu'),
          ),
        ),
        body: SafeArea(
            child: ListView.builder(
                itemCount: 9,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      store.getImage('assets/images/cat$index.png');
                      Navigator.pushNamed(context, '/ProductDetails');
                      // Navigator.pushNamed(context, 'underTest');
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('plant name and Details'),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(FeatherIcons.box,
                                              color: Colors.teal),
                                          Text('Available')
                                        ],
                                      ),
                                      SizedBox(
                                        width: 50,
                                      ),
                                      Row(
                                        children: [
                                          Text('4.3'),
                                          Icon(
                                            FeatherIcons.star,
                                            color: Colors.amber,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Icon(
                                        FeatherIcons.dollarSign,
                                        color: Colors.green[700],
                                      ),
                                      Text(
                                        '110',
                                        style: TextStyle(
                                            fontFamily: 'Menu', fontSize: 22),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Image.asset(
                                    'assets/images/cat$index.png',
                                    height: 100,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  ProductProperty(),
                                ],
                              )
                            ],
                          ),
                          Divider(
                            color: Colors.black38,
                            // endIndent: 10,
                            // indent: 10,
                          )
                        ],
                      ),
                    ),
                  );
                })),
      );
    });
  }
}

class ProductProperty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          padding: EdgeInsets.all(3),
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: Colors.green.withOpacity(.9),
                width: 3.0,
                style: BorderStyle.solid),
          ),
          child: Icon(
            Icons.wb_sunny,
            color: Colors.green.withOpacity(.9),
          ),
        ),
        Container(
          padding: EdgeInsets.all(3),
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: Colors.green.withOpacity(.9),
                width: 3.0,
                style: BorderStyle.solid),
          ),
          child: Icon(
            Icons.ac_unit,
            color: Colors.green.withOpacity(.9),
          ),
        ),
        Container(
          padding: EdgeInsets.all(3),
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: Colors.green.withOpacity(.9),
                width: 3.0,
                style: BorderStyle.solid),
          ),
          child: Icon(
            Icons.spa,
            color: Colors.green.withOpacity(.9),
          ),
        )
      ],
    );
  }
}
