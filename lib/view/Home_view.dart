import 'package:flutter/material.dart';
import 'package:green_pot/Mobx/mainState.dart';
import 'package:mobx_provider/mobx_provider.dart';
import 'package:pit_slider_carousel/pit_slider_carousel.dart';

class HomeArea extends StatefulWidget {
  @override
  _HomeAreaState createState() => _HomeAreaState();
}

class _HomeAreaState extends State<HomeArea> {
  CarouselController ctrl;

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Image.asset(
                        'assets/images/gp.png',
                        height: 40,
                      ),
                      Text(
                        'Green Pot',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  IconButton(icon: Icon(Icons.search), onPressed: () {})
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView(
                children: [
                  _CarouselSlider(),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            top: 15, bottom: 15, left: 10, right: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(
                              color: Colors.black.withOpacity(.9),
                              width: 3.0,
                              style: BorderStyle.solid),
                        ),
                        child: Text('Seed'),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 10, right: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(
                                color: Colors.black.withOpacity(.9),
                                width: 3.0,
                                style: BorderStyle.solid),
                          ),
                          child: Text('flower')),
                      Container(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 10, right: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(
                                color: Colors.black.withOpacity(.9),
                                width: 3.0,
                                style: BorderStyle.solid),
                          ),
                          child: Text('tree'))
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Top Picks',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text('See more'),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 370,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Card_pattern_first(
                          index: 1,
                        ),
                        Card_pattern_first(
                          index: 2,
                        ),
                        Card_pattern_first(
                          index: 3,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Image.asset('assets/images/3.jpg'),
                  Container(
                    padding: EdgeInsets.only(top: 25, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'The other one',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text('See more'),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 365,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Card_pattern_secend(
                          index: 1,
                          onTab: () {
                            // Navigator.pushNamed(context, '/ProductDetails');
                          },
                        ),
                        Card_pattern_secend(
                          index: 2,
                        ),
                        Card_pattern_secend(
                          index: 3,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    
  }

//*------------------------- this is my slide show widget setting ---------
//-                        * * * * * * * * * * * * * * * * * * * *
  Widget _CarouselSlider() {
    List<CarouselItem> list = [];
    for (int i = 0; i < 5; i++) {
      list.add(CarouselItem(Image.asset('assets/images/${i}.jpg')));
    }

    ctrl = CarouselController(carouselItems: list);

    return new PitSliderCarousel(
        maxDotsIndicator: 10,
        dotSize: 6.0,
        activeDotColor: Colors.amber,
        dotColor: Colors.black,
        useDot: true,
        animationCurve: Curves.easeInCubic,
        autoPlay: true,
        dotPosition: Position(bottom: -10.0),
        carouselController: ctrl);
  }
} //!   End of HomeView Class

// ----------------- this is my Cart Horizontal list Type < one > --------------
// -                 * * * * * * * * * * * * * * * *
class Card_pattern_first extends StatelessWidget {
  final index;
  const Card_pattern_first({
    this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      color: Colors.green.withOpacity(.8),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            Container(
                width: 190,
                child: Text(
                  'small',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.end,
                )),
            Container(
                width: 190,
                child: Text(
                  '\$25',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.end,
                )),
            Image.asset(
              'assets/images/p$index.png',
              height: 200,
            ),
            Container(
                width: 130,
                child: Text(
                  'Gift box',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.start,
                )),
            Container(
                width: 130,
                child: Text(
                  'Aloe Vera',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                  textAlign: TextAlign.start,
                )),
            SizedBox(
              height: 10,
            ),
            Container(
              width: 180,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                          color: Colors.white.withOpacity(.3),
                          width: 3.0,
                          style: BorderStyle.solid),
                    ),
                    child: Icon(
                      Icons.wb_sunny,
                      color: Colors.white.withOpacity(.5),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                          color: Colors.white.withOpacity(.3),
                          width: 3.0,
                          style: BorderStyle.solid),
                    ),
                    child: Icon(
                      Icons.ac_unit,
                      color: Colors.white.withOpacity(.5),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                          color: Colors.white.withOpacity(.3),
                          width: 3.0,
                          style: BorderStyle.solid),
                    ),
                    child: Icon(
                      Icons.spa,
                      color: Colors.white.withOpacity(.5),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

// *---------------- this is my Cart Horizontal list Type < two > --------------
// -                * * * * * * * * * * * * * * * * * * * * * * *
class Card_pattern_secend extends StatelessWidget {
  final index;
  final Function onTab;
  const Card_pattern_secend({
    this.onTab,
    this.index,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTab,
      child: Card(
        elevation: 4,
        // color: Colors.green.withOpacity(.1),
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            // Container(
            //     width: 190,
            //     child: Text(
            //       'small',
            //       style: TextStyle(
            //         color: Colors.white,
            //         fontSize: 15,
            //       ),
            //       textAlign: TextAlign.end,
            //     )),

            Image.asset(
              'assets/images/p$index.png',
              height: 200,
            ),
            Material(
              color: Colors.green,
              shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(6),
                    topRight: Radius.circular(30)),
              ),
              child: Container(
                width: 200,
                child: Column(
                  children: [
                    SizedBox(
                      height: 7,
                    ),
                    Container(
                        width: 130,
                        child: Text(
                          'Gift box',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.start,
                        )),
                    Container(
                        width: 130,
                        child: Text(
                          'Aloe Vera',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                          ),
                          textAlign: TextAlign.start,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: 180,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.white.withOpacity(.3),
                                  width: 3.0,
                                  style: BorderStyle.solid),
                            ),
                            child: Icon(
                              Icons.wb_sunny,
                              color: Colors.white.withOpacity(.5),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.white.withOpacity(.3),
                                  width: 3.0,
                                  style: BorderStyle.solid),
                            ),
                            child: Icon(
                              Icons.ac_unit,
                              color: Colors.white.withOpacity(.5),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.white.withOpacity(.3),
                                  width: 3.0,
                                  style: BorderStyle.solid),
                            ),
                            child: Icon(
                              Icons.spa,
                              color: Colors.white.withOpacity(.5),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 7,
                    )
                  ],
                ),
              ),
            ),
            Container(
              width: 200,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text('  '),
                      Material(
                        color: Colors.teal[900],
                        shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(0),
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                        ),
                        child: Container(
                            padding:
                                EdgeInsets.only(top: 7, bottom: 7, left: 15),
                            // color: Colors.black,
                            width: 100,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '\$',
                                  style: TextStyle(
                                      color: Colors.red[300],
                                      fontSize: 15,
                                      fontFamily: 'Menu'),
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  '25',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Menu',
                                    fontSize: 20,
                                  ),
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                  Text('small   ')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
