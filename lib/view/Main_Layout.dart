//    -- Dependencies Library --
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:green_pot/Web_services/api.dart';

import 'package:green_pot/model/Product_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

//    -- Views Widget --
import 'package:green_pot/view/Cart_view.dart';
import 'package:green_pot/view/Categories_view.dart';
import 'package:green_pot/view/Home_view.dart';
import 'package:green_pot/view/Profile_view.dart';
import 'package:green_pot/view/Save_view.dart';

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

// -- This Widget is the main layout witch handel the view widgets
class _MainViewState extends State<MainView> {
// __________________________________________________________________________
//!    ++++++++++++++++++ -- Future Api -- +++++++++++++++++++
// __________________________________________________________________________
  Future _feedData() async {
    var data = await http.get(products);
    var jsonData = jsonDecode(data.body);
    // List<Product> feeds = [];
    // for (var f in jsonData) {
    //   Product feed = Product(
    //     name: f["heading"],
    //   );
    //   feeds.add(feed);
    // }
    String name = jsonData["type"];
    return name;
  }

//! -----------------  end Future Api --------------------------------
// ____________________________________________________________________

  // This is Page index State
  int _selectedItemPosition = 2;
  final List<Widget> _views = [
    ProfileView(),
    SaveView(),
    HomeArea(),
    CategoriesView(),
    CartView()
  ];
  @override
  Widget build(BuildContext context) {
    print(_feedData());
    return Scaffold(
      body: _views[_selectedItemPosition],
      bottomNavigationBar: SnakeNavigationBar(
        style: SnakeBarStyle.pinned,
        selectedItemColor: Colors.lightGreen[600],
        snakeShape: SnakeShape.rectangle,
        snakeColor: Colors.black,
        backgroundColor: Colors.lightGreen.withAlpha(100),
// padding: EdgeInsets.only(bottom: 5,left: 0,right: 0),
        // shape: BeveledRectangleBorder(
        //   borderRadius: BorderRadius.only(
        //       topLeft: Radius.circular(25), topRight: Radius.circular(25)),
        // ),
        // elevation: 5.0,
        showUnselectedLabels: false,
        showSelectedLabels: true,
        // shape: bottomBarShape,
        // padding: padding,
        currentIndex: _selectedItemPosition,
        onPositionChanged: (index) =>
            setState(() => _selectedItemPosition = index),

        items: [
          _buildBottomNavigationBarItem(
            icon: FeatherIcons.user,
            title: 'Profile',
          ),
          _buildBottomNavigationBarItem(
            icon: FeatherIcons.heart,
            title: 'Save',
          ),
          _buildBottomNavigationBarItem(
            icon: FeatherIcons.home,
            title: 'Home',
          ),
          _buildBottomNavigationBarItem(
            icon: FeatherIcons.grid,
            title: 'Categories',
          ),
          _buildBottomNavigationBarItem(
            icon: FeatherIcons.shoppingCart,
            title: 'Shop list',
          ),
        ],
      ),
    );
  }

  //*        --- Bottom NavBAr ItemBuilder ---
  BottomNavigationBarItem _buildBottomNavigationBarItem(
      {@required IconData icon, @required String title}) {
    return BottomNavigationBarItem(
        icon: Icon(
          icon,
          size: 25,
        ),
        title: Text(
          title,
          style: TextStyle(fontFamily: 'Menu', fontSize: 10),
        ));
  }
}
