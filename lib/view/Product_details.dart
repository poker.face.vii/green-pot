
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:green_pot/Mobx/mainState.dart';
import 'package:green_pot/Utility/Buy_Floating_BTN.dart';
import 'package:green_pot/Utility/Product_Details_Clipper.dart';
import 'package:mobx_provider/mobx_provider.dart';

class ProductDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {Provider.of<MainState>(context, listen: false);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return MobxStatefulProvider<MainState>(builder: (context, store) {
      return Scaffold(
       
        floatingActionButton: _buyFloatingActionButton(),
        body: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              clipperShadow(),
              Header(image: store.image,),
              _scrollArea(width),
            ],
          ),
        ),
      );
    });
  }

  SafeArea _scrollArea(double width) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 320),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'About this product',
              style: TextStyle(fontFamily: 'Menu', fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: width - 45,
              child: Flexible(
                child: Text(
                  'They form naked or tunicless scaly underground bulbs which are their organs of perennation. In some North American species the base of the bulb develops into rhizomes, on which numerous small bulbs are found. Some species develop stolons. Most bulbs are buried deep in the ground, but a few species form bulbs near the soil surface',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 140,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Card(
                    color: Color(0xffdbf5f3),
                    elevation: 10.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Column(
                        children: [
                          Icon(
                            WeatherIcons.raindrop,
                            size: 40,
                            color: Colors.lightGreen[700],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            width: 70,
                            child: Flexible(
                              child: Text('Twice a week',
                                  style: TextStyle(
                                      color: Colors.lightGreen[700],
                                      fontFamily: 'Menu',
                                      fontSize: 18)),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Card(
                    color: Color(0xffdbf5f3),
                    elevation: 10.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Column(
                        children: [
                          Icon(
                            WeatherIcons.thermometer,
                            color: Colors.lightGreen,
                            size: 40,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            '20 C',
                            style: TextStyle(
                                color: Colors.lightGreen,
                                fontFamily: 'Menu',
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Card(
                    color: Color(0xffdbf5f3),
                    elevation: 10.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Column(
                        children: [
                          Icon(
                            WeatherIcons.day_sunny,
                            color: Colors.lightGreen,
                            size: 40,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            'many',
                            style: TextStyle(
                                color: Colors.lightGreen,
                                fontFamily: 'Menu',
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Available Color',
              style: TextStyle(
                  color: Colors.black, fontFamily: 'Menu', fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 35,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: colorList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: colorList[index],
                          borderRadius: BorderRadius.circular(50)),
                      width: 15,
                      height: 10,
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }

  final List<Color> colorList = [
    Colors.amber,
    Colors.red,
    Colors.blue,
    Colors.brown,
    Colors.teal
  ];
  Stack _buyFloatingActionButton() {
    return Stack(
      children: [
        Positioned(
          bottom: 7,
          right: 0,
          child: ClipPath(
            clipper: BuyFloatingActionButton(),
            child: Container(
              height: 40,
              child: FlatButton(
                color: Colors.teal,
                onPressed: () {},
                child: Text(
                  'Buy',
                  style: TextStyle(
                      fontFamily: 'Menu', fontSize: 18, color: Colors.white),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

//______________________________________________________________________________
//*  -->>   The Shadow Of the Clipper   <<--
//______________________________________________________________________________
  Positioned clipperShadow() {
    return Positioned(
        child: ClipPath(
      clipper: ProductDetailClipper(),
      child: Container(
        height: 347,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [
                0.6,
                .9,
              ],
              colors: [
                // Colors.black,
                Color(0xff24481e),
                Colors.white
              ]),
        ),
      ),
    ));
  }

//______________________________________________________________________________
//*->>   This is the Header of this View Until the Clipper With green Color   <<
//______________________________________________________________________________
}

class Header extends StatelessWidget {
  final String image;

   Header({@required this.image});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 0,
        left: 0,
        right: 0,
        child: ClipPath(
          clipper: ProductDetailClipper(),
          child: Container(
            // color: Color(0xff24481e),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  // stops: [0.2,.6, .9,],
                  colors: [
                    Color(0xff24481e),
                    // Color(0xff3b8050),
                    Colors.teal[700]
                  ]),
            ),
            height: 340,
            width: double.infinity,
            child: SafeArea(
                child: Column(
              children: [
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white.withOpacity(.3)),
                              child: Icon(
                                Icons.chevron_left,
                                color: Colors.white,
                                size: 25,
                              )),
                        ),
                        Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white.withOpacity(.3)),
                            child: Icon(
                              FeatherIcons.moreHorizontal,
                              color: Colors.white,
                              size: 25,
                            )),
                      ],
                    )),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('product name',
                                style: TextStyle(
                                  fontFamily: 'Menu',
                                  color: Colors.white,
                                  fontSize: 15,
                                )),
                            SizedBox(
                              height: 15,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  FeatherIcons.dollarSign,
                                  color: Colors.green[200],
                                ),
                                Text(
                                  '170',
                                  style: TextStyle(
                                      fontFamily: 'Menu',
                                      fontSize: 25,
                                      color: Colors.white),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text('shipping fee',
                                style: TextStyle(
                                  fontFamily: 'Menu',
                                  color: Colors.white.withOpacity(.5),
                                  fontSize: 12,
                                )),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  FeatherIcons.dollarSign,
                                  color: Colors.green[200],
                                  size: 20,
                                ),
                                Text(
                                  '20',
                                  style: TextStyle(
                                      fontFamily: 'Menu',
                                      fontSize: 18,
                                      color: Colors.white),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text('shop',
                                style: TextStyle(
                                  fontFamily: 'Menu',
                                  color: Colors.white.withOpacity(.5),
                                  fontSize: 12,
                                )),
                            Text('Blue Farm',
                                style: TextStyle(
                                  fontFamily: 'Menu',
                                  color: Colors.white,
                                  fontSize: 15,
                                )),
                          ]),
                      Material(
                        elevation: 16.0,
                        child: Image.asset(
                          image,
                          width: 150,
                        ),
                      )
                    ],
                  ),
                )
              ],
            )),
          ),
        ));
  }
}
