import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  int _selectedItemPosition = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(top: 20, left: 15, right: 15),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(children: [
                  Icon(
                    FeatherIcons.bell,
                  ),
                  Positioned(
                    top: -13,
                    right: 3,
                    child: Container(
                        width: 8,
                        child: CircleAvatar(
                          backgroundColor: Colors.red,
                        )),
                  )
                ]),
                Icon(FeatherIcons.settings)
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Center(
              child: Text(
                'Sajjad PokerFace',
                style: TextStyle(fontFamily: 'Menu', fontSize: 20),
              ),
            ),
            Center(
              child: Text(
                '0903 275 58 25',
                style: TextStyle(
                    fontFamily: 'Menu', fontSize: 15, color: Colors.black54),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Icon(
                        Icons.monetization_on,color: Colors.yellowAccent[700],
                        size: 40,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text('Wallet',
                          style: TextStyle(
                              fontFamily: 'Menu',
                              fontSize: 14,
                              color: Colors.black))
                    ],
                  ),
                  VerticalDivider(
                    indent: 4,
                    endIndent: 4,
                    color: Colors.black54,
                    width: 2,
                  ),
                  Column(
                    children: [
                      Icon(
                        FeatherIcons.shoppingBag,color: Colors.teal[800],
                        size: 40,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text('My orders',
                          style: TextStyle(
                              fontFamily: 'Menu',
                              fontSize: 14,
                              color: Colors.black))
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
