import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:green_pot/Mobx/mainState.dart';
import 'package:mobx/mobx.dart';
import 'package:mobx_provider/mobx_provider.dart';
import 'package:provider/provider.dart';

class Test extends StatelessWidget {
  void addStore(BuildContext context) {}

  @override
  Widget build(BuildContext context) {
    final store = Provider.of<MainState>(context, listen: false);

    return Observer(
        builder: (_) {
      return Scaffold(
        body: Container(
          child: Center(
            child: GestureDetector(
                onTap: () {
                  store.selected = 'ppp';
                  Navigator.pushNamed(context, 'underTest');
                },
                child: Text(store.selected)),
          ),
        ),
      );
    });
  }
}

class UnderTest extends StatelessWidget {
  void addStore(BuildContext context) {}

  @override
  Widget build(BuildContext context) {
    // final store = Provider.of<MainState>(context, listen: false);

    return MobxStatefulProvider<MainState>(
        builder: (BuildContext context, store) {
      return Scaffold(
          body: Container(
        child: Center(
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
            child: Text(store.selected),
          ),
        ),
      ));
    });
  }
}
